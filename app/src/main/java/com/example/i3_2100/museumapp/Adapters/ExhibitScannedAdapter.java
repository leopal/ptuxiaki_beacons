package com.example.i3_2100.museumapp.Adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.i3_2100.museumapp.Activities.ScanningActivity;
import com.example.i3_2100.museumapp.HelpingClasses.Exhibit;
import com.example.i3_2100.museumapp.R;

/*
    adapter gia ta exhibit elements
 */

public class ExhibitScannedAdapter extends BaseAdapter {

    private Exhibit[] myArray;
    private static LayoutInflater inflater=null;
    Context context;



    public ExhibitScannedAdapter(Exhibit[] temp, ScanningActivity scanningActivity) {
        this.myArray =temp;
        context= scanningActivity;
        inflater = ( LayoutInflater )context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return myArray.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder
    {
        TextView tv;
        ImageView img;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Holder holder = new Holder();
        View rowView = convertView;
            if (rowView == null) {
                rowView = inflater.inflate(R.layout.beacons_list_item, null);
            }
            holder.tv = (TextView) rowView.findViewById(R.id.exhibit_textview);
            holder.img = (ImageView) rowView.findViewById(R.id.exhibit_image);

            holder.tv.setText(myArray[position].getExhibit_name());
            holder.img.setImageDrawable(myArray[position].getImg_id());

            return rowView;

    }



}
