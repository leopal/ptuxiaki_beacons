package com.example.i3_2100.museumapp.Database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class BeaconsDbHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "museumdatabase.db";
    private static final int DB_VERSION = 24;

    /*tables*/

    private static final String BEACONS_TABLE = "beacons";
    private static final String EXHIBIT_TABLE = "exhibit";
    private static final String THEME_TABLE = "theme";
    private static final String CONTENT_TABLE = "content";

    /*
    Table Beacons attributes
     */
    public static final String BEACONS_INSTANCE_COLUMN = "instance";

    /*
    Table Exhibit attributes
     */
    public static final String EXHIBIT_ID_COLUMN = "_id";
    public static final String EXHIBIT_TITLE_COLUMN = "title";
    public static final String EXHIBIT_ROOM_COLUMN = "room";
    public static final String EXHIBIT_PICTURE_COLUMN = "picture_path";
    public static final String EXHIBIT_CONTENT_COLUMN = "content_id";

    /*
    Table Theme attributes
     */
    public static final String THEME_ID_COLUMN = "_id";
    public static final String THEME_TITLE_COLUMN = "title";

    /*
    Table Content attributes
     */
    public static final String CONTENT_ID_COLUMN = "_id";
    public static final String CONTENT_TEXT_COLUMN = "textpath";
    public static final String CONTENT_AUDIO_COLUMN = "audiopath";
    public static final String CONTENT_PHOTO_COLUMN = "photopath";
    public static final String CONTENT_VIDEO_COLUMN = "videopath";
    public static final String CONTENT_GALLERY_COLUMN = "gallerypath";
    public static final String CONTENT_THEME_COLUMN = "theme_id";

    /*
    Tables creation queries
     */
    public static final String CREATE_BEACONS_TABLE = "CREATE TABLE " +BEACONS_TABLE+ "("+ BEACONS_INSTANCE_COLUMN + " TEXT NOT NULL  PRIMARY KEY)";
    public static final String CREATE_THEME_TABLE = "CREATE TABLE " +THEME_TABLE+ "(" + THEME_ID_COLUMN + " INTEGER NOT NULL PRIMARY KEY, " + THEME_TITLE_COLUMN + " TEXT NOT NULL)";
    public static final String CREATE_EXHIBIT_TABLE = "CREATE TABLE " +EXHIBIT_TABLE + "(" +EXHIBIT_PICTURE_COLUMN+ " TEXT NOT NULL, "+ EXHIBIT_ROOM_COLUMN + " INTEGER," +
            EXHIBIT_TITLE_COLUMN + " TEXT NOT NULL, "+EXHIBIT_CONTENT_COLUMN+ " INTEGER NOT NULL,"+ EXHIBIT_ID_COLUMN + " TEXT NOT NULL,PRIMARY KEY("+EXHIBIT_ID_COLUMN+" , "+EXHIBIT_CONTENT_COLUMN+") FOREIGN KEY(" +EXHIBIT_ID_COLUMN+ ") REFERENCES "+ BEACONS_TABLE+ "("+BEACONS_INSTANCE_COLUMN+"),FOREIGN KEY(" +EXHIBIT_CONTENT_COLUMN+ ") REFERENCES "+ CONTENT_TABLE +"("+CONTENT_ID_COLUMN+"))";
    public static final String CREATE_CONTENT_TABLE = "CREATE TABLE " +CONTENT_TABLE+ "("+ CONTENT_ID_COLUMN+ " INTEGER NOT NULL, "+ CONTENT_AUDIO_COLUMN + " TEXT ,"
            + CONTENT_PHOTO_COLUMN + " TEXT ," + CONTENT_TEXT_COLUMN + " TEXT ," +CONTENT_VIDEO_COLUMN+ " TEXT ," + CONTENT_GALLERY_COLUMN + " TEXT ," + CONTENT_THEME_COLUMN+ " INTEGER ,FOREIGN KEY(" + CONTENT_THEME_COLUMN+ ") REFERENCES "+THEME_TABLE+ "("+THEME_ID_COLUMN+"))" ;

    private static BeaconsDbHelper instance;

    public static synchronized BeaconsDbHelper getHelper(Context context)
    {
        if (instance == null)
            instance = new BeaconsDbHelper(context);
        return instance;
    }

    private BeaconsDbHelper(Context context)
    {
        super(context,DB_NAME,null,DB_VERSION);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            // Enable foreign key constraints
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }


    public static String getExhibitTable() {
        return EXHIBIT_TABLE;
    }

    public static String getThemeTable() {
        return THEME_TABLE;
    }

    public static String getContentTable() {
        return CONTENT_TABLE;
    }

    /*
    dimiourgia tables
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_BEACONS_TABLE);
        db.execSQL(CREATE_THEME_TABLE);
        db.execSQL(CREATE_EXHIBIT_TABLE);
        db.execSQL(CREATE_CONTENT_TABLE);

        /*
        inserts in Beacons Table
         */
        String sql = "INSERT INTO " + BEACONS_TABLE + "(" + BEACONS_INSTANCE_COLUMN +") VALUES('d6e89f5dfe25')";
        db.execSQL(sql);
        sql = "INSERT INTO " + BEACONS_TABLE + "(" + BEACONS_INSTANCE_COLUMN +") VALUES('c20bd604b5f6')";
        db.execSQL(sql);

        /*
        inserts in Themes Table
         */
        sql = "INSERT INTO " + THEME_TABLE + "(" + THEME_ID_COLUMN + ","+ THEME_TITLE_COLUMN +") VALUES(1,'Τέχνη')";
        db.execSQL(sql);
        sql = "INSERT INTO " + THEME_TABLE + "(" + THEME_ID_COLUMN + ","+ THEME_TITLE_COLUMN +") VALUES(2,'Η Ιστορία του Πανεπιστημίου')";
        db.execSQL(sql);
        sql = "INSERT INTO " + THEME_TABLE + "(" + THEME_ID_COLUMN + ","+ THEME_TITLE_COLUMN +") VALUES(3,'Ο Δημιουργός')";
        db.execSQL(sql);

        /*
        inserts in Exhibit Table
        */
        sql = "INSERT INTO " + EXHIBIT_TABLE + "(" + EXHIBIT_ID_COLUMN + "," + EXHIBIT_ROOM_COLUMN + "," + EXHIBIT_PICTURE_COLUMN + "," + EXHIBIT_TITLE_COLUMN + "," + EXHIBIT_CONTENT_COLUMN +
                ") VALUES ('d6e89f5dfe25',0,'id2/orfanidis.jpeg','Η προσωπογραφία του Θεόδωρου Ορφανίδη',11)";
        db.execSQL(sql);
        sql = "INSERT INTO " + EXHIBIT_TABLE + "(" + EXHIBIT_ID_COLUMN + "," + EXHIBIT_ROOM_COLUMN + "," + EXHIBIT_PICTURE_COLUMN + "," + EXHIBIT_TITLE_COLUMN + "," + EXHIBIT_CONTENT_COLUMN +
                ") VALUES ('d6e89f5dfe25',0,'id2/orfanidis.jpeg','Η προσωπογραφεία του Θεόδωρου Ορφανίδη',14)";
        db.execSQL(sql);
        sql = "INSERT INTO " + EXHIBIT_TABLE + "(" + EXHIBIT_ID_COLUMN + "," + EXHIBIT_ROOM_COLUMN + "," + EXHIBIT_PICTURE_COLUMN + "," + EXHIBIT_TITLE_COLUMN + "," + EXHIBIT_CONTENT_COLUMN +
                ") VALUES ('c20bd604b5f6',0,'id1/lavaro.jpg','Το Λάβαρο του Πανεπιστημίου',10)";
        db.execSQL(sql);
        sql = "INSERT INTO " + EXHIBIT_TABLE + "(" + EXHIBIT_ID_COLUMN + "," + EXHIBIT_ROOM_COLUMN + "," + EXHIBIT_PICTURE_COLUMN + "," + EXHIBIT_TITLE_COLUMN + "," + EXHIBIT_CONTENT_COLUMN +
                ") VALUES ('c20bd604b5f6',0,'id1/lavaro.jpg','Το Λάβαρο του Πανεπιστημίου',12)";
        db.execSQL(sql);
        sql = "INSERT INTO " + EXHIBIT_TABLE + "(" + EXHIBIT_ID_COLUMN + "," + EXHIBIT_ROOM_COLUMN + "," + EXHIBIT_PICTURE_COLUMN + "," + EXHIBIT_TITLE_COLUMN + "," + EXHIBIT_CONTENT_COLUMN +
                ") VALUES ('c20bd604b5f6',0,'id1/lavaro.jpg','Το Λάβαρο του Πανεπιστημίου',13)";
        db.execSQL(sql);


        /*
        inserts in Content Table
        */

        sql = "INSERT INTO " + CONTENT_TABLE + "("+ CONTENT_ID_COLUMN + "," + CONTENT_THEME_COLUMN + "," + CONTENT_VIDEO_COLUMN + "," + CONTENT_TEXT_COLUMN + "," + CONTENT_PHOTO_COLUMN + "," + CONTENT_AUDIO_COLUMN + "," + CONTENT_GALLERY_COLUMN +
                ") VALUES (10,1,NULL,'themes/theme1/text/lavaro_text_texni.html','id1/lavaro.jpg','themes/theme1/audio/kritikes.mp3',NULL)";
        db.execSQL(sql);
        sql = "INSERT INTO " + CONTENT_TABLE + "("+ CONTENT_ID_COLUMN + "," + CONTENT_THEME_COLUMN + "," + CONTENT_VIDEO_COLUMN + "," + CONTENT_TEXT_COLUMN + "," + CONTENT_PHOTO_COLUMN + "," + CONTENT_AUDIO_COLUMN + "," + CONTENT_GALLERY_COLUMN +
                ") VALUES (14,2,NULL,'themes/theme2/text/orfanidis_text_istoria.html',NULL,'themes/theme2/audio/af48.mp3','themes/theme2/gallery')";
        db.execSQL(sql);
        sql = "INSERT INTO " + CONTENT_TABLE + "("+ CONTENT_ID_COLUMN + "," + CONTENT_THEME_COLUMN + "," + CONTENT_VIDEO_COLUMN + "," + CONTENT_TEXT_COLUMN + "," + CONTENT_PHOTO_COLUMN + "," + CONTENT_AUDIO_COLUMN + "," + CONTENT_GALLERY_COLUMN +
                ") VALUES (11,1,NULL,'themes/theme1/text/orfanidis_text_texni.html','themes/theme1/photo/orfanidis_vivlio.jpg','themes/theme1/audio/orfanidis.mp3',NULL)";
        db.execSQL(sql);
        sql = "INSERT INTO " + CONTENT_TABLE + "("+ CONTENT_ID_COLUMN + "," + CONTENT_THEME_COLUMN + "," + CONTENT_VIDEO_COLUMN + "," + CONTENT_TEXT_COLUMN + "," + CONTENT_PHOTO_COLUMN + "," + CONTENT_AUDIO_COLUMN + "," + CONTENT_GALLERY_COLUMN +
                ") VALUES (13,2,'symvola_new',NULL,'id1/lavaro.jpg',NULL,NULL)";
        db.execSQL(sql);
        sql = "INSERT INTO " + CONTENT_TABLE + "("+ CONTENT_ID_COLUMN + "," + CONTENT_THEME_COLUMN + "," + CONTENT_VIDEO_COLUMN + "," + CONTENT_TEXT_COLUMN + "," + CONTENT_PHOTO_COLUMN + "," + CONTENT_AUDIO_COLUMN + "," + CONTENT_GALLERY_COLUMN +
                ") VALUES (12,3,NULL,'themes/theme3/text/lavaro_text_creator.html','themes/theme3/photo/gizis.jpeg',NULL,NULL)";
        db.execSQL(sql);



    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + BEACONS_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + THEME_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + EXHIBIT_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + CONTENT_TABLE);
        // create new tables
        onCreate(db);
    }
}
