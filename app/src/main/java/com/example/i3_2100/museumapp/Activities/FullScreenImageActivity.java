package com.example.i3_2100.museumapp.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.example.i3_2100.museumapp.Adapters.SlidingImageAdapter;
import com.example.i3_2100.museumapp.Fragments.PlaceholderFragment;
import com.example.i3_2100.museumapp.R;

import java.util.ArrayList;
import java.util.Collections;

/*
    klasi pou xrisimoeitai gia na dei o xristis tis foto sto gallery se megali othoni
 */

public class FullScreenImageActivity extends AppCompatActivity {


    private ArrayList<Drawable> ImagesArray = new ArrayList<>();
    private  Drawable[] IMAGES;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_image_activity);
        Intent i = getIntent();
        int position = i.getExtras().getInt(PlaceholderFragment.getGalleryImagePosition());
        ExhibitDetails_Tabbed.setSelected_tab(i.getExtras().getInt(PlaceholderFragment.getSelectedTabIndex()));
        IMAGES = PlaceholderFragment.drawable;
        init(position);

    }


    private void init(int position) {
        ViewPager mPager;
        Collections.addAll(ImagesArray, IMAGES);
        mPager = (ViewPager) findViewById(R.id.pager);
        if (mPager!=null)
            {
            mPager.setBackgroundColor(Color.BLACK);
            mPager.setAdapter(new SlidingImageAdapter(this,ImagesArray));
            mPager.setCurrentItem(position);
            }
        }



    }

