package com.example.i3_2100.museumapp.Fragments;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.example.i3_2100.museumapp.Activities.FullScreenImageActivity;
import com.example.i3_2100.museumapp.Adapters.GalleryAdapter;
import com.example.i3_2100.museumapp.Database.BeaconsDbHelper;
import com.example.i3_2100.museumapp.HelpingClasses.ExpandableGridView;
import com.example.i3_2100.museumapp.R;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


public class PlaceholderFragment extends Fragment {

    private TextView exhibit_info;
    private BeaconsDbHelper mDatabase;
    private ImageButton play_button;
    private ImageButton stop_button;
    private VideoView mvideo;
    private MediaPlayer audioMediaPlayer;
    private SeekBar mSeekBar;
    private ExpandableGridView gridView;
    private Handler mHandler = new Handler();
    private static final  String CONTENT_ID = "content_id";
    private static final  String GALLERY_IMAGE_POSITION = "gallery_image_position";
    private static final  String SELECTED_TAB_INDEX = "selected_tab_index";
    private ArrayList<String> listImages;
    public static Drawable[] drawable;
    private static int content_id;
    private ImageView exhibit_image;
    private MediaController mediaController;
    private static int selected_tab_number;



    public PlaceholderFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PlaceholderFragment newInstance(int sectionNumber,int id) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        final Bundle args = new Bundle();
        content_id = id;
        args.putInt(CONTENT_ID,content_id);
        fragment.setArguments(args);
        selected_tab_number = sectionNumber;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_exhibit_details__tabbed, container, false);
        initializeFunction(rootView);
        Bundle bundle = getArguments();
        loadContet(bundle.getInt(CONTENT_ID),rootView);
        return rootView;
    }


    @Override
    public void onStop() {
        super.onStop();
        if (audioMediaPlayer !=null)
            if (audioMediaPlayer.isPlaying())
                PauseAudioPlayback();
        if (mvideo.isPlaying())
            mvideo.stopPlayback();
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (audioMediaPlayer !=null)
            if (audioMediaPlayer.isPlaying())
                PauseAudioPlayback();
        if (mvideo.isPlaying())
            mvideo.stopPlayback();
        // audioMediaPlayer = null;
    }

    private String readTxt(String path) {

        InputStream inputStream = null;
        try {
            inputStream = getActivity().getApplicationContext().getAssets().open(path);
            int size = inputStream.available();

            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();

            String str = new String(buffer);
            return str;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "ERROR IN READTEXT";
    }

    private Drawable getDrawbleFromString(String str)
    {
        AssetManager assetManager = getActivity().getApplicationContext().getAssets();
        InputStream ims = null;
        try {
            ims = assetManager.open(str);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Drawable d = Drawable.createFromStream(ims, null);
        return d;


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser)
        {
            if( audioMediaPlayer !=null)
                if(audioMediaPlayer.isPlaying())
                    PauseAudioPlayback();
            if ( mvideo != null)
                if ( mvideo.isPlaying())
                mvideo.pause();
            if (mediaController!=null)
                mediaController.hide();
        }
    }

    public static String getGalleryImagePosition() {
        return GALLERY_IMAGE_POSITION;
    }

    public static String getSelectedTabIndex() {
        return SELECTED_TAB_INDEX;
    }

    private void loadContet(int _id, final View rootView)
    {
                               /*opening database*/
        mDatabase = BeaconsDbHelper.getHelper(getActivity().getApplicationContext());
        SQLiteDatabase db = mDatabase.getReadableDatabase();
        audioMediaPlayer = new MediaPlayer();

        /*
        fetch content id from database
         */
        String query_to_content_table = "SELECT * FROM "+ mDatabase.getContentTable()+ " WHERE "+mDatabase.CONTENT_ID_COLUMN+ " =?";
        Cursor c = db.rawQuery(query_to_content_table, new String[]{String.valueOf(_id)});

        if(c!=null && c.moveToFirst())
        {


                /*get paths*/
            final String textpath = c.getString(c.getColumnIndex(mDatabase.CONTENT_TEXT_COLUMN));
            String imagepath = c.getString(c.getColumnIndex(mDatabase.CONTENT_PHOTO_COLUMN));
            String audiopath = c.getString(c.getColumnIndex(mDatabase.CONTENT_AUDIO_COLUMN));
            String videopath = c.getString(c.getColumnIndex(mDatabase.CONTENT_VIDEO_COLUMN));
            String gallerypath = c.getString(c.getColumnIndex(mDatabase.CONTENT_GALLERY_COLUMN));



            if (textpath!=null)
                exhibit_info.setText(Html.fromHtml(readTxt(textpath)));
            else
                setTextUnusedElementsGone(rootView);
            if (imagepath!=null)
                exhibit_image.setImageDrawable(getDrawbleFromString(imagepath));
            else
                setImageUnusedElementsGone(rootView);
            if (gallerypath!=null)
            {
                CreateGalleryFromAssets(gallerypath);
                gridView.setAdapter(new GalleryAdapter(getActivity(),drawable));
                gridView.setExpanded(true);

                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent intent = new Intent(getActivity().getApplicationContext(),FullScreenImageActivity.class);
                        intent.putExtra(GALLERY_IMAGE_POSITION,position);
                        intent.putExtra(SELECTED_TAB_INDEX, selected_tab_number);
                        startActivity(intent);

                    }
                });
            }
            else
                setGalleryElementsGone(rootView);

            if(videopath!=null)
                prepareVideoForPlayback(videopath);
            else
                setVideoUnusedElementsGone(rootView);

            if(audiopath!=null)
            {
                audioPlayback(audiopath,audioMediaPlayer);
                audioMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.seekTo(0);
                        mSeekBar.setProgress(0);
                        play_button.setImageResource(R.drawable.play);
                    }
                });
                play_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (audioMediaPlayer!=null)
                            if (  audioMediaPlayer.isPlaying()) {
                                PauseAudioPlayback();
                            } else {
                                StartAudioPlayback();
                                mHandler.removeCallbacks(run);
                                mHandler.postDelayed(run, 100);
                            }
                    }
                });

                stop_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (audioMediaPlayer != null) {
                            audioMediaPlayer.seekTo(0);
                            mSeekBar.setProgress(0);
                            if (audioMediaPlayer.isPlaying())
                                PauseAudioPlayback();
                        }
                    }
                });

                mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if (audioMediaPlayer != null && fromUser)
                            audioMediaPlayer.seekTo(progress);

                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });
            }
            else
            {
                setAudioUnusedElementsGone(rootView);
            }




        }

    }



    private void setImageUnusedElementsGone(View rootView) {

        LinearLayout temp = (LinearLayout) rootView.findViewById(R.id.image_elements);
        temp.setVisibility(View.INVISIBLE);
        View gridView = rootView.findViewById(R.id.exhibit_image);
        gridView.setVisibility(View.INVISIBLE);

    }



    private void prepareVideoForPlayback(String videopath)
    {
        getActivity().getWindow().setFormat(PixelFormat.TRANSLUCENT);
        String uri = "android.resource://"+getActivity().getPackageName()+"/" +getResources().getIdentifier(videopath, "raw", getActivity().getPackageName());;


        /*
        setting videoview dimensions
        */
        MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
        metaRetriever.setDataSource(getActivity().getApplicationContext(), Uri.parse(uri));
        String height = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
        String width = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mvideo.getLayoutParams();
        params.height = Integer.parseInt(height);
        mvideo.setLayoutParams(params);

        /*
        prepare video and video buttons for playback
        */
        mediaController = new MediaController(getActivity(),false);
        mediaController.requestFocus();


        mvideo.setMediaController(mediaController);
        mvideo.setVideoURI(Uri.parse(uri));
        mvideo.seekTo(100);
        mvideo.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (mvideo.isPlaying())
                    mvideo.pause();
                else
                    mvideo.start();
                return false;
            }
        });

        mvideo.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mvideo.seekTo(100);
            }
        });
        mvideo.setZOrderOnTop(true);
        mvideo.requestFocus();


    }



    private void CreateGalleryFromAssets(String gallerypath) {
        try {
            String[] images =getActivity().getAssets().list(gallerypath);
            listImages = new ArrayList<>(images.length);
            for (String x :images)
            {
                listImages.add(x);
            }
            InputStream inputstream;
            drawable = new Drawable[listImages.size()];

            for (int i=0;i<listImages.size();i++)
            {
                inputstream=getActivity().getApplicationContext().getAssets().open(gallerypath+"/"+listImages.get(i));
                drawable[i]= Drawable.createFromStream(inputstream, null);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setGalleryElementsGone(View rootView) {
        View textview = rootView.findViewById(R.id.gallery_section);
        textview.setVisibility(View.INVISIBLE);
        LinearLayout temp = (LinearLayout) rootView.findViewById(R.id.gallery_elements);
        temp.setVisibility(View.INVISIBLE);
        View gridView = rootView.findViewById(R.id.gridView);
        gridView.setVisibility(View.INVISIBLE);

    }

    private void audioPlayback(String path, MediaPlayer player)
    {
        AssetFileDescriptor afd;
        try {
            afd = getActivity().getAssets().openFd(path);
            player.setDataSource(afd.getFileDescriptor(),afd.getStartOffset(),afd.getLength());
            afd.close();
            player.prepare();
            mSeekBar.setMax(player.getDuration());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void StartAudioPlayback()
    {
        play_button.setImageResource(R.drawable.pause);
        play_button.setScaleType(ImageView.ScaleType.FIT_XY);
        audioMediaPlayer.start();
    }

    private void PauseAudioPlayback()
    {
        play_button.setImageResource(R.drawable.play);
        play_button.setScaleType(ImageView.ScaleType.FIT_XY);
        audioMediaPlayer.pause();
    }


    public void seekUpdation() {
        mSeekBar.setProgress(audioMediaPlayer.getCurrentPosition());
        mHandler.postDelayed(run, 1000);
    }
    Runnable run = new Runnable() {
        @Override
        public void run() {
            if( audioMediaPlayer !=null)
                if(audioMediaPlayer.isPlaying())
                    seekUpdation();
        }
    };

    private void setAudioUnusedElementsGone(View rootView)
    {

        View plbtn = rootView.findViewById(R.id.audio_section);
        plbtn.setVisibility(View.GONE);
        RelativeLayout temp = (RelativeLayout) rootView.findViewById(R.id.audio_elements);
        temp.setVisibility(View.GONE);
    }

    private void setVideoUnusedElementsGone(View rootView)
    {

        View plbtn = rootView.findViewById(R.id.video_section);
        plbtn.setVisibility(View.GONE);
        LinearLayout temp = (LinearLayout) rootView.findViewById(R.id.video_elements);
        temp.setVisibility(View.GONE);
    }

    private void setTextUnusedElementsGone(View rootView)
    {
        View plbtn = rootView.findViewById(R.id.exhibit_info);
        plbtn.setVisibility(View.GONE);
        LinearLayout temp = (LinearLayout) rootView.findViewById(R.id.text_elements);
        temp.setVisibility(View.GONE);
    }

    private void initializeFunction(View rootView)
    {
        exhibit_info = (TextView) rootView.findViewById(R.id.exhibit_info);
        play_button = (ImageButton) rootView.findViewById(R.id.play_pause_button);
        play_button.setImageResource(R.drawable.play);
        play_button.setScaleType(ImageView.ScaleType.FIT_CENTER);

        stop_button = (ImageButton) rootView.findViewById(R.id.stop_button);
        stop_button.setImageResource(R.drawable.stop);
        stop_button.setScaleType(ImageView.ScaleType.FIT_XY);

        mSeekBar = (SeekBar) rootView.findViewById(R.id.audio_seekbar);
        mvideo = (VideoView) rootView.findViewById(R.id.video_screen);

        gridView = (ExpandableGridView) rootView.findViewById(R.id.gridView);

        exhibit_image = (ImageView) rootView.findViewById(R.id.exhibit_image);




    }
}