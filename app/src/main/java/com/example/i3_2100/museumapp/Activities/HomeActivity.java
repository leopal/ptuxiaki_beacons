package com.example.i3_2100.museumapp.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.estimote.sdk.SystemRequirementsChecker;
import com.example.i3_2100.museumapp.Adapters.HomePageListViewAdapter;
import com.example.i3_2100.museumapp.HelpingClasses.HomeButtonArray;
import com.example.i3_2100.museumapp.R;

public class HomeActivity extends AppCompatActivity {

    private GridView homeListView;
    private HomeButtonArray[] buttonArray;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        createButtonArray();
        initializeFunction();

        homeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position==0 )
                {
                    Intent intent = new Intent(HomeActivity.this, HistoryActivity.class);
                    startActivity(intent);
                }
                if (position==1 )
                    if (SystemRequirementsChecker.checkWithDefaultDialogs(HomeActivity.this))
                    {
                        Intent intent2 = new Intent(HomeActivity.this, ScanningActivity.class);
                        startActivity(intent2);
                    }
                    else
                        toastMessage();

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.threedotmenu:
                startThemeSelector();
                return true;
            default:
                return false;
        }
    }

    private void toastMessage() {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, R.string.enable_bluetooth_message, duration);
        toast.show();
    }

    /*
    dimiourgei ta buttons stin arxiki othoni
     */
    private void createButtonArray() {

        this.buttonArray = new HomeButtonArray[2];
        this.buttonArray[0] = new HomeButtonArray(R.drawable.history,R.string.button_history);
        this.buttonArray[1] = new HomeButtonArray(R.drawable.exploring,R.string.button_beacon);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_themes, menu);
        return true;
    }

    /*
sunartisi initialization ton xml elements
 */
    private void initializeFunction()
    {
        HomePageListViewAdapter homePageListViewAdapter;
        homeListView = (GridView) findViewById(R.id.home_page_listview);
        homePageListViewAdapter = new HomePageListViewAdapter(getApplicationContext(),buttonArray);
        homeListView.setAdapter(homePageListViewAdapter);

    }

    public void startThemeSelector()
    {
        Intent i = new Intent(this,ThemeSelector.class);
        startActivity(i);
    }

}
