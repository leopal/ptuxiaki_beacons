package com.example.i3_2100.museumapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.example.i3_2100.museumapp.R;


/*
    adapter gia ta gallery items
 */
public class GalleryAdapter extends BaseAdapter {

    private Context context;
    public static Drawable[] images;

    public GalleryAdapter(Context context,Drawable[] array) {

        this.context = context;
        this.images = array;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public Object getItem(int position) {
        return images[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public  Drawable[] getImages() {
        return images;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(context);
        imageView.setImageDrawable(images[position]);
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
         imageView.setLayoutParams(new GridView.LayoutParams(240,240));
        return imageView;
    }
}
