package com.example.i3_2100.museumapp.Fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.estimote.sdk.eddystone.Eddystone;
import com.example.i3_2100.museumapp.Activities.ExhibitDetails_Tabbed;
import com.example.i3_2100.museumapp.Activities.ScanningActivity;
import com.example.i3_2100.museumapp.Adapters.ExhibitScannedAdapter;
import com.example.i3_2100.museumapp.Database.BeaconsDbHelper;
import com.example.i3_2100.museumapp.HelpingClasses.Exhibit;
import com.example.i3_2100.museumapp.R;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


public class BeacListFragment extends Fragment {

    private ListView beaconList;
    private ArrayList<Eddystone> mylist;
    private TextView mytext;
    private Exhibit[] myExhibitArray;
    private ExhibitScannedAdapter myAdapter=null;
    Context context;
    private BeaconsDbHelper mDatabase;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    /*
    sunartisis gia to fragment me ta ekthemata pou vrike o scanner
     */

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.beacon_list_fragment,container,false);
        beaconList = (ListView) v.findViewById(R.id.beacons_listview);
        context = this.getActivity();
        Bundle extras = getArguments();

        /*opening database*/
        mDatabase = BeaconsDbHelper.getHelper(context);



        if (!extras.isEmpty())
            {
                mylist = extras.getParcelableArrayList("Eddystones");
                if (mylist != null && !mylist.isEmpty()) {
                    myExhibitArray=createArray(mylist,mDatabase);
                    if(myAdapter==null)
                    {
                        myAdapter = new ExhibitScannedAdapter(myExhibitArray, (ScanningActivity) this.getActivity());
                        beaconList.setAdapter(myAdapter);
                    }
                    else
                    {
                        myAdapter.notifyDataSetChanged();
                    }
                }
                else if (mylist!=null && mylist.isEmpty())
                {
                    mytext = (TextView) v.findViewById(R.id.nothing_found);
                    mytext.setText(R.string.nothing_found);
                }

            }

        beaconList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(),ExhibitDetails_Tabbed.class);
                intent.putExtra("INSTANCE",mylist.get(position).instance);
                startActivity(intent);
            }
        });



        return  v;
    }





    private Exhibit[] createArray(ArrayList<Eddystone> mylist,BeaconsDbHelper mDatabase)
    {
        String query,dbString,dbPhoto;
        int i;
        Cursor c;
        SQLiteDatabase db = mDatabase.getReadableDatabase();

        for(i = 0;i<mylist.size();i++) {
            query = "SELECT * FROM " + mDatabase.getExhibitTable() + " WHERE " + mDatabase.EXHIBIT_ID_COLUMN + "=?";
            c = db.rawQuery(query, new String[]{mylist.get(i).instance});
            Log.i("WTF","TX power is"+mylist.get(i).calibratedTxPower);
            if (c.getCount()==0)
                mylist.remove(i);
        }

        Exhibit[] myExhibitArray = new Exhibit[mylist.size()];
        for(i = 0;i<mylist.size();i++) {
            query = "SELECT * FROM " + mDatabase.getExhibitTable() + " WHERE "+ mDatabase.EXHIBIT_ID_COLUMN +"=?";
            c = db.rawQuery(query, new String[]{mylist.get(i).instance});
            if (c != null && c.moveToFirst() )
            {
                dbString = c.getString(c.getColumnIndex(mDatabase.EXHIBIT_TITLE_COLUMN));
                dbPhoto = c.getString(c.getColumnIndex(mDatabase.EXHIBIT_PICTURE_COLUMN));
                myExhibitArray[i] = new Exhibit(dbString,getDrawbleFromString(dbPhoto));
                c.close();
            }
        }
        return myExhibitArray;
    }

    private Drawable getDrawbleFromString(String str)
    {
        AssetManager assetManager = context.getAssets();
        InputStream inputStream = null;
        try {
            inputStream = assetManager.open(str);
        } catch (IOException e) {
            e.printStackTrace();
        }

            return Drawable.createFromStream(inputStream, null);


    }



}
