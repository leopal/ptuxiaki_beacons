package com.example.i3_2100.museumapp.Activities;


import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.SystemRequirementsChecker;
import com.estimote.sdk.eddystone.Eddystone;
import com.example.i3_2100.museumapp.Fragments.BeacListFragment;
import com.example.i3_2100.museumapp.R;

import java.util.ArrayList;
import java.util.List;

public class ScanningActivity extends AppCompatActivity implements Parcelable {

    private ImageButton scan_button ;
    private BeaconManager beaconManager;
    private String scanId2;
    private TextView status;
    private int button_state=0;
    private ArrayList<Eddystone> mlist;
    final FragmentManager fragmentManager = getFragmentManager();
    private static final String KEY1 = "BUTTON_STATE";
    Handler  handler = new Handler();
    private  Fragment fragment;
    private boolean isEnabled;
    private  int  [] imageArray = {R.drawable.bluetooth0,R.drawable.bluetooth,R.drawable.bluetooth2};

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt(KEY1,button_state);
    }


    private void initializeFunction()
    {
        scan_button = (ImageButton) findViewById(R.id.scan_button);
        beaconManager  = new BeaconManager(this);
        status = (TextView) findViewById(R.id.status);
    }


    public ScanningActivity() {
    }

    protected ScanningActivity(Parcel in) {
        scanId2 = in.readString();
        button_state = in.readInt();
        mlist = in.createTypedArrayList(Eddystone.CREATOR);
    }

    public static final Creator<ScanningActivity> CREATOR = new Creator<ScanningActivity>() {
        @Override
        public ScanningActivity createFromParcel(Parcel in) {
            return new ScanningActivity(in);
        }

        @Override
        public ScanningActivity[] newArray(int size) {
            return new ScanningActivity[size];
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.beacon_home);

        initializeFunction();
        //beaconManager.setBackgroundScanPeriod(5,2);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Bundle bundle =new Bundle();
        mlist = new ArrayList<>();
        if (button_state==0)
            scanId2=beaconManager.startEddystoneScanning();
        scan_button.setImageResource(R.drawable.bluetooth0);
        startRunnable();

        if(savedInstanceState!=null)
        {
            button_state = savedInstanceState.getInt(KEY1);
            if(button_state==1)
            {
                beaconManager.stopEddystoneScanning(scanId2);
                status.setText(R.string.stopped);
                scan_button.setImageResource(R.drawable.ic_bluetooth_disabled_black_48dp);
                isEnabled = false;

            }
            else
            {
                //button_state = 1;
                scanId2=beaconManager.startEddystoneScanning();
                status.setText(R.string.scanning);
                isEnabled = true;
            }

        }
        scan_button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(button_state==0)
                {

                    button_state=1;
                    status.setText(R.string.stopped);
                    scan_button.setImageResource(R.drawable.ic_bluetooth_disabled_black_48dp);
                    BluetoothIconChange();
                    beaconManager.stopEddystoneScanning(scanId2);


                }
                else
                {
                    button_state=0;
                    status.setText(R.string.scanning);
                    BluetoothIconChange();
                    scanId2=beaconManager.startEddystoneScanning();


                }


            }
        });

        //dimiourgia/ananeosei tou fragment pou periexei ta ekthemata pou einai konta

        beaconManager.setEddystoneListener(new BeaconManager.EddystoneListener() {
            @Override
            public void onEddystonesFound(List<Eddystone> eddystoneList) {
               fragment = fragmentManager.findFragmentById(R.id.fragment_container);
                if(fragment!=null && !mlist.equals(eddystoneList))
                {
                    fragment = new BeacListFragment();
                    fragment.setArguments(bundle);
                    fragmentManager.beginTransaction().replace(R.id.fragment_container,fragment).commitAllowingStateLoss();
                }
                if (fragment == null && !bundle.isEmpty())
                {
                    fragment = new BeacListFragment();
                    fragment.setArguments(bundle);
                    fragmentManager.beginTransaction().add(R.id.fragment_container, fragment).commitAllowingStateLoss();
                }
                mlist.clear();
                mlist.addAll(eddystoneList);
                bundle.putParcelableArrayList("Eddystones", mlist);

            }
        });
    }


    private void startRunnable()
    {
        isEnabled = true;
        handler.postDelayed(runnable,500);

    }

    private void stopRunnable()
    {
        isEnabled = false;
        handler.removeCallbacks(runnable);
    }
    private void BluetoothIconChange(){

        if (isEnabled)
        {
            stopRunnable();
        }
        else
        {
            startRunnable();
        }
    }


    /*updating every second the bluetooth icon*/
    Runnable runnable = new Runnable() {

        int i = 0;
        public void run() {
            if(isEnabled) {
                scan_button.setImageResource(imageArray[i]);
                i++;
                if (i > imageArray.length - 1) {
                    i = 0;
                }
                handler.postDelayed(runnable, 500);
            }
            else
            {
                Thread.currentThread().interrupt();
            }

        }

    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(mlist);
    }




    @Override
    public void onStart()
    {
        super.onStart();
        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override public void onServiceReady() {
                                if (button_state==0)
                                    scanId2 = beaconManager.startEddystoneScanning();
                                else
                                    beaconManager.stopEddystoneScanning(scanId2);
            }
        });
    }

    @Override
    public void  onResume()
    {
        super.onResume();
        SystemRequirementsChecker.checkWithDefaultDialogs(this);

    }

    @Override
    public void onStop()
    {
        super.onStop();
        beaconManager.stopEddystoneScanning(scanId2);

    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        beaconManager.stopEddystoneScanning(scanId2);
        beaconManager.disconnect();
        fragment =fragmentManager.findFragmentById(R.id.fragment_container);
        if(fragment!=null)
        {
            fragmentManager.beginTransaction().remove(fragment).commit();
        }


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.threedotmenu:
                Intent i = new Intent(this,ThemeSelector.class);
                startActivity(i);
                return true;
            case android.R.id.home:

                this.finish();
                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_themes, menu);
        return true;
    }

}