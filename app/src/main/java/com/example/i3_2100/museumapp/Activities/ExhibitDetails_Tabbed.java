package com.example.i3_2100.museumapp.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import com.example.i3_2100.museumapp.Adapters.SectionsPagerAdapter;
import com.example.i3_2100.museumapp.Database.BeaconsDbHelper;
import com.example.i3_2100.museumapp.R;

import java.util.Arrays;

public class ExhibitDetails_Tabbed extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private BeaconsDbHelper mDatabase;
    private String instance;
    private String[] themeArray;
    private String[] themeNameArray;
    private int[] contentArray;
    private SQLiteDatabase db;
    private Toolbar toolbar;
    private ImageButton filter_button;
    private CoordinatorLayout activeLayout;
    private static final String NO_THEME_TAG = "no_theme";
    private static final String INSTANCE_TAG = "INSTANCE";
    private  TabLayout tabLayout;
    private static int selected_tab = -1;



    public static void setSelected_tab(int selected_tab) {
        ExhibitDetails_Tabbed.selected_tab = selected_tab;
    }

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exhibit_details__tabbed);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                                       //opening database
        mDatabase = BeaconsDbHelper.getHelper(getApplicationContext());
        db = mDatabase.getReadableDatabase();
        instance = getIntent().getStringExtra(INSTANCE_TAG);
        findContent(db);
        if (themeArray.length == 0)
        {
            setContentView(R.layout.exhibit_details_no_theme);
            toolbar = (Toolbar) findViewById(R.id.toolbar_no_theme);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            filter_button = (ImageButton) findViewById(R.id.filter_imageButton);
            filter_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ExhibitDetails_Tabbed.this,ThemeSelector.class);
                    startActivity(intent);

                }
            });
        }

        else{

            // Create the adapter that will return a fragment for each of the
            // primary sections of the activity.
            activeLayout = (CoordinatorLayout) findViewById(R.id.main_content);
            if (activeLayout.getTag().equals(NO_THEME_TAG))
                setContentView(R.layout.activity_exhibit_details__tabbed);


            mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), contentArray,themeNameArray);
            // Set up the ViewPager with the sections adapter.
            mViewPager = (ViewPager) findViewById(R.id.container);
            mViewPager.setAdapter(mSectionsPagerAdapter);
            mViewPager.setOffscreenPageLimit(themeArray.length);
            tabLayout = (TabLayout) findViewById(R.id.tabs);
            tabLayout.setupWithViewPager(mViewPager);




        }


    }

    /*
    sunartisi pou kaluptei tin periptosi pou o xristis epileksei nees thematikes..ksanadimiourgei
    to activity oste na sumfonei me tis nees thematikes
     */

    private void reCreateActivityOnThemeChange(){
        setContentView(R.layout.activity_exhibit_details__tabbed);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                            //opening database
        mDatabase = BeaconsDbHelper.getHelper(getApplicationContext());
        db = mDatabase.getReadableDatabase();
        instance = getIntent().getStringExtra(INSTANCE_TAG);
        findContent(db);
        if (themeArray.length == 0 ||  contentArray.length == 0)
        {
            setContentView(R.layout.exhibit_details_no_theme);
            toolbar = (Toolbar) findViewById(R.id.toolbar_no_theme);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            filter_button = (ImageButton) findViewById(R.id.filter_imageButton);
            filter_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ExhibitDetails_Tabbed.this,ThemeSelector.class);
                    startActivity(intent);

                }
            });
        }

        else{

            // Create the adapter that will return a fragment for each of the
            // primary sections of the activity.

            mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), contentArray,themeNameArray);
            // Set up the ViewPager with the sections adapter.
            mViewPager = (ViewPager) findViewById(R.id.container);
            mViewPager.setAdapter(mSectionsPagerAdapter);
            mViewPager.setOffscreenPageLimit(themeArray.length);
            TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
            tabLayout.setupWithViewPager(mViewPager);



        }
    }




    @Override
    protected void onResume() {
        super.onResume();
        reCreateActivityOnThemeChange();


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_themes, menu);
        return true;
    }


/*
    ektelei ta aparaitita queries sti vasi oste na fortothei to periexomeno pou prepei
 */
    private  void findContent(SQLiteDatabase db)
    {
        /*
        get which themes user has selected
         */
        Cursor cursor;
        String query_themes = "SELECT * FROM " +mDatabase.getThemeTable();
        cursor = db.rawQuery(query_themes,null);
        int counter =0;
        SharedPreferences myPreference=PreferenceManager.getDefaultSharedPreferences(this);
        if (cursor!=null && cursor.moveToFirst())
        {
            do
            {


                if (myPreference.getBoolean((cursor.getString(cursor.getColumnIndex(mDatabase.THEME_TITLE_COLUMN))),false))
                {
                    counter++;
                }
            }while (cursor.moveToNext());
            cursor.moveToFirst();
            themeArray = null;
            themeArray = new String[counter];

            counter = 0;
            do
            {
                if (myPreference.getBoolean((cursor.getString(cursor.getColumnIndex(mDatabase.THEME_TITLE_COLUMN))),false))
                {
                    themeArray[counter] = cursor.getString(cursor.getColumnIndex(mDatabase.THEME_ID_COLUMN));
                    counter++;
                }
            }while (cursor.moveToNext());



        }

        /*
        fetch content for the beacon that is selected
         */
        int[] tempArray;
        String query_exhibit = "SELECT " +mDatabase.EXHIBIT_CONTENT_COLUMN + " FROM "+mDatabase.getExhibitTable()+
                " WHERE " +mDatabase.EXHIBIT_ID_COLUMN+ " = ?";
        cursor = db.rawQuery(query_exhibit, new String[]{instance});
        counter = 0;
        tempArray = new int[cursor.getCount()];


        if (cursor!=null && cursor.moveToFirst())
        {

            do
            {
                tempArray[counter] = cursor.getInt(cursor.getColumnIndex(mDatabase.EXHIBIT_CONTENT_COLUMN));
                counter++;
            }while (cursor.moveToNext());
        }


        String themeInClause = modifyStringArrayforQuery(themeArray);

        /*
        filter content depending on themes
         */
        int[] tempArray2;

        String query_content;

        query_content= "SELECT "+mDatabase.CONTENT_ID_COLUMN+ " FROM "+mDatabase.getContentTable()+
                    " WHERE "+mDatabase.CONTENT_THEME_COLUMN+ " IN "+themeInClause;
        cursor = db.rawQuery(query_content, null);
        tempArray2 = new int[cursor.getCount()];

        if (cursor!=null && cursor.moveToFirst())
        {
            counter = 0;
            do
            {
                tempArray2[counter] = cursor.getInt(cursor.getColumnIndex(mDatabase.CONTENT_ID_COLUMN));
                counter++;
            }while (cursor.moveToNext());
        }

        createContentArray(tempArray,tempArray2);

        themeNameArray = new String[contentArray.length];
        int[] temp = new int[contentArray.length];
        query_content = "SELECT "+mDatabase.CONTENT_THEME_COLUMN+ " FROM "+mDatabase.getContentTable()+
                " WHERE "+ mDatabase.CONTENT_ID_COLUMN +" = ?";
        for (int i =0;i<contentArray.length;i++)
        {
            cursor = db.rawQuery(query_content,new String[]{String.valueOf(contentArray[i])});
            if (cursor!=null && cursor.moveToFirst())
            {
                temp[i] = cursor.getInt(cursor.getColumnIndex(mDatabase.CONTENT_THEME_COLUMN));

            }

        }

        query_content = "SELECT "+mDatabase.THEME_TITLE_COLUMN+ " FROM "+mDatabase.getThemeTable()+
                " WHERE "+ mDatabase.THEME_ID_COLUMN +" = ?";
        for (int i =0;i<temp.length;i++)
        {
            cursor = db.rawQuery(query_content,new String[]{String.valueOf(temp[i])});
            if (cursor!=null && cursor.moveToFirst())
            {
                themeNameArray[i] = cursor.getString(cursor.getColumnIndex(mDatabase.THEME_TITLE_COLUMN));
            }

        }


        cursor.close();
    }

    /*
    voithitiki sunartisi, dimiourgei ton teliko contentArray pou periexei to periexomeno pros provoli
     */
    private void createContentArray(int[] arr1,int [] arr2)
    {
        int cnt = 0,k = 0;
        for(int i=0;i<arr1.length;i++){
            for(int j=0;j<arr2.length;j++) {
                if (arr1[i] == arr2[j]) {
                    cnt++;
                    }
                }
            }
        contentArray = null;
        contentArray = new int[cnt];
            for(int i=0;i<arr1.length;i++){
                for(int j=0;j<arr2.length;j++) {
                    if (arr1[i] == arr2[j])
                        contentArray[k++] = arr1[i];
                    }

        }
    }

    /*
    voithitiki sunartisi, morfopoiei ta periexomena enos String[] array oste na mporei
    na sumperilifthei se query
     */
    private String modifyStringArrayforQuery(String[] array)
    {
        String inClause = Arrays.toString(array);

    //at this point inClause will look like "[23,343,33,55,43]"
    //replace the brackets with parentheses
        inClause = inClause.replace("[","(");
        inClause = inClause.replace("]",")");
        return inClause;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;
            case R.id.threedotmenu:
                Intent i = new Intent(this,ThemeSelector.class);
                startActivity(i);
                return true;
            default:
                return false;
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (selected_tab >= 0 )
        {
            mViewPager.setCurrentItem(selected_tab);
            setSelected_tab(-1);
        }
    }
}
