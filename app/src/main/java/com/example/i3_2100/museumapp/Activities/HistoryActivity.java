package com.example.i3_2100.museumapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.i3_2100.museumapp.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class HistoryActivity extends AppCompatActivity {


     /*
     sunartisi initialization ton xml elements
      */

    private void initializeFunction()
    {
        TextView history;
        history = (TextView) findViewById(R.id.history_text);
        if (history!=null)
        {
            history.setText(Html.fromHtml(readTxt()));
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar!=null)
        {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history);
        initializeFunction();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.threedotmenu:
                Intent i = new Intent(HistoryActivity.this,ThemeSelector.class);
                startActivity(i);
                return true;
        }
        return super.onOptionsItemSelected(item);


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_themes, menu);
        return true;
    }

    private String readTxt() {
        InputStream inputStream = getResources().openRawResource(R.raw.history);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i;
        try {
            i = inputStream.read();
            while (i != -1)
            {
                byteArrayOutputStream.write(i);
                i = inputStream.read();
            }
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return byteArrayOutputStream.toString();
    }
}
