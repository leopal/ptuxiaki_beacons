package com.example.i3_2100.museumapp.Activities;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.i3_2100.museumapp.Database.BeaconsDbHelper;
import com.example.i3_2100.museumapp.HelpingClasses.AppCompatPreferenceActivity;
import com.example.i3_2100.museumapp.R;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class ThemeSelector extends AppCompatPreferenceActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new MyPreferenceFragment()).commit();
    }

    public static class MyPreferenceFragment extends PreferenceFragment
    {

        private BeaconsDbHelper mDatabase;
        private SQLiteDatabase db;
        private static PreferenceScreen mPreferenceScreen;


        @Override
        public void onCreate(final Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            mPreferenceScreen = myfunct();
            setPreferenceScreen(mPreferenceScreen);


        }

        /*
        dimiourgei ta checkboxes me tis thematikes enotites
         */

        private PreferenceScreen myfunct()
        {
            //create preference screen
            PreferenceScreen root = getPreferenceManager().createPreferenceScreen(getActivity());

            //fetch themes list from database
             mDatabase = BeaconsDbHelper.getHelper(getActivity().getApplicationContext());

            db = mDatabase.getReadableDatabase();
            String query = "SELECT * FROM "+mDatabase.getThemeTable();
            Cursor cursor = db.rawQuery(query,null);

            if (cursor!=null && cursor.moveToFirst())
            {

                    do {
                        CheckBoxPreference checkBoxPreference = new CheckBoxPreference(getActivity());
                        checkBoxPreference.setKey(cursor.getString(cursor.getColumnIndex(mDatabase.THEME_TITLE_COLUMN)));
                        checkBoxPreference.setTitle(cursor.getString(cursor.getColumnIndex(mDatabase.THEME_TITLE_COLUMN)));
                        checkBoxPreference.setDefaultValue(true);
                        root.addPreference(checkBoxPreference);
                    } while (cursor.moveToNext());
                cursor.close();
            }


            toastMessage();

            return root;

        }



        private void toastMessage() {
            Context context = getActivity().getApplicationContext();
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, R.string.theme_selection_hint, duration);
            toast.show();
        }

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }



}
