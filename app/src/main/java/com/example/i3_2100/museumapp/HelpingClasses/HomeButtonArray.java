package com.example.i3_2100.museumapp.HelpingClasses;


public class HomeButtonArray {

    private int stringid;
    private Integer image;

    public HomeButtonArray(Integer image, int text) {
        this.image = image;
        this.stringid = text;
    }

    public int getText() {

        return stringid;
    }

    public void setText(int text) {
        this.stringid = text;
    }

    public Integer getImage() {
        return image;
    }

}
