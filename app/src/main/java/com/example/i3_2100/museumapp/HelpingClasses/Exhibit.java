package com.example.i3_2100.museumapp.HelpingClasses;


import android.graphics.drawable.Drawable;

public class Exhibit {

    private String exhibit_name;
    private Drawable img_id;


    public Exhibit(String str,Drawable id)
    {
        this.exhibit_name=str;
        this.img_id=id;
    }
    public String getExhibit_name() {
        return exhibit_name;
    }

    public Drawable getImg_id() {
        return img_id;
    }




}
