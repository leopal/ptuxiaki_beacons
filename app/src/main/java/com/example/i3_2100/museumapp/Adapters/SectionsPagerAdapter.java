package com.example.i3_2100.museumapp.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.i3_2100.museumapp.Fragments.PlaceholderFragment;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */

/*
    adapter gia ta tabs me to periexomeno
 */
public  class SectionsPagerAdapter extends FragmentStatePagerAdapter {

    private int[] contentArray;
    private String[] themeArray;



    public SectionsPagerAdapter(FragmentManager fm, int[] array, String[] theme_ar) {
        super(fm);
        this.contentArray = array;
        this.themeArray = theme_ar;

    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        return PlaceholderFragment.newInstance(position +1,contentArray[position]);

    }



    @Override
    public int getCount() {
        return contentArray.length;
    }



    @Override
    public CharSequence getPageTitle(int position) {

        return themeArray[position];
    }
}