package com.example.i3_2100.museumapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.i3_2100.museumapp.HelpingClasses.HomeButtonArray;
import com.example.i3_2100.museumapp.R;


/*
    adapter gia ta home screen buttons
 */
public class HomePageListViewAdapter extends BaseAdapter {



    private HomeButtonArray[] items;
    private Context context;
    private static LayoutInflater inflater=null;

    public HomePageListViewAdapter(Context context,HomeButtonArray[] items) {
        this.context = context;
        this.items = items;
        inflater = ( LayoutInflater )context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder = new Holder();
        View rowView = convertView;
        if(rowView==null)
        {
            rowView = inflater.inflate(R.layout.home_buttons, null);
        }
        holder.tv = (TextView) rowView.findViewById(R.id.home_page_textview);
        holder.img=(ImageView) rowView.findViewById(R.id.home_page_imageview);
        holder.tv.setText(this.items[position].getText());
        holder.img.setImageResource(this.items[position].getImage());
        return rowView;
    }

    private class Holder {
        TextView tv;
        ImageView img;
    }
}
